import sequtils
import arraymancer
import math
import sparse

#https://github.com/Jutho/KrylovKit.jl
#https://en.wikipedia.org/wiki/Lanczos_algorithm
#https://jutho.github.io/KrylovKit.jl/stable/man/eig/

proc eigs_lanczos*[T: SomeFloat](A: Tensor[T], m: int): Tensor[T] =
    assert rank(A) == 2 and A.shape[0] == A.shape[1]
    # Get size of A
    let N = A.shape[0]
    # Storage for old v's
    var
        vlist: seq[Tensor[T]]
        alist: seq[T]
        blist: seq[T]
    # Generate random initial vector of norm 1
    var v = randomTensor[T](N, 1f)
    v/=sqrt(dot(v, v))
    vlist.add(v)
    # Initial iteration step
    var wprime = A*v
    var a = dot(wprime, v)
    var w = wprime-a*v
    var beta = T(0.0)
    alist.add(a)
    blist.add(beta)
    for j in 2..m:
        beta = sqrt(dot(w, w))
        if beta != 0:
            v = w/beta
        else:
            # new v as arbitrary vector with Euclidean norm 1 that is orthogonal to all of prior v
            v = randomTensor[T](N, 1f)
            for vprior in vlist:
                v-=vprior*dot(v, vprior)
            v/=sqrt(dot(v, v))
        wprime = A*v
        a = dot(wprime, v)
        # w=wprime-a*v-beta*vlist[vlist.high]
        w = wprime
        w-=(a*v)
        w-=(beta*vlist[vlist.high])
        vlist.add(v)
        alist.add(a)
        blist.add(beta)
    # Create matrix of Krylov vectors
    let V = vlist.stack()
    # Create symmetric tridiagonal matrix T
    var Tmat = zeros[T](m, m)
    for i in alist.low..alist.high:
        Tmat[i, i] = alist[i]
    for i in 1..blist.high:
        if i > 0:
            Tmat[i, i-1] = blist[i]
            Tmat[i-1, i] = blist[i]
    # Solve for eigenvectors of T matrix
    let esol = symeig(Tmat, return_eigenvectors = true)
    let
        eigenval = esol[0]
        eigenvec = esol[1]
    # Transform eigenvectors from Krylov space to original
    #TODO: convert into eigenvectors of A y as y=V*x
    #TODO: is this correct? (order, tranpose, etc)
    return transpose(V)*eigenvec

#TODO: copy above and implement for SparseMatrix

proc eigs_lanczos*[T: SomeFloat, U](A: SparseMatrix[U, T],
        maxiter0: int = 1_000, nev: int = 6): (Tensor[T], Tensor[T]) =
    assert A.nx == A.ny
    # Get size of A
    let N = A.nx
    #TODO: is this reasonable?
    let maxiter = min(maxiter0, N)
    # Storage for old v's
    var
        vlist: seq[SparseVector[U, T]]
        alist: seq[T]
        blist: seq[T]
    # Generate random initial vector of norm 1
    var vten = randomTensor[T](N, 1f)
    vten/=sqrt(dot(vten, vten))
    var v = newSparseVector[U, T](N, A.blocksize)
    v.fromTensor(vten)
    vlist.add(v)
    # Initial iteration step
    var wprime = A*v
    var a = dot(wprime, v)
    var w = wprime-v*a
    var beta = T(0.0)
    alist.add(a)
    blist.add(beta)
    for j in 2..maxiter:
        beta = sqrt(dot(w, w))
        if beta != 0:
            v = w*(1/beta)
        else:
            # new v as arbitrary vector with Euclidean norm 1 that is orthogonal to all of prior v
            vten = randomTensor[T](N, 1f)
            v.fromTensor(vten)
            for vprior in vlist:
                v-=vprior*dot(v, vprior)
            v*=(1/sqrt(dot(v, v)))
        wprime = A*v
        a = dot(wprime, v)
        # w=wprime-a*v-beta*vlist[vlist.high]
        w = wprime
        w-=(v*a)
        w-=(vlist[vlist.high]*beta)
        vlist.add(v)
        alist.add(a)
        blist.add(beta)
    # Create matrix of Krylov vectors
    var kvlist: seq[Tensor[T]]
    for kv in vlist:
        kvlist.add(kv.toTensor())
    let V = kvlist.stack()
    # Create symmetric tridiagonal matrix T
    var Tmat = zeros[T](maxiter, maxiter)
    for i in alist.low..alist.high:
        Tmat[i, i] = alist[i]
    for i in 1..blist.high:
        if i > 0:
            Tmat[i, i-1] = blist[i]
            Tmat[i-1, i] = blist[i]
    # Solve for eigenvectors of T matrix
    echo 0..<nev
    let esol = symeig(Tmat, slice = 0..<nev, return_eigenvectors = true)
    let
        eigenval = esol[0]
        eigenvec = esol[1]
    # Transform eigenvectors from Krylov space to original
    #TODO: convert into eigenvectors of A y as y=V*x
    #TODO: is this correct? (order, tranpose, etc)
    #TODO: # of v as maxiter, nev as ?
    return (eigenval, transpose(V)*eigenvec)

proc eigs_ritz*[T: SomeFloat, U](A: SparseMatrix[U, T], nritz0: int = 30,
        nev: int = 6): (Tensor[T], Tensor[T]) =
    assert A.nx == A.ny
    # Get size of A
    let N = A.nx
    #TODO: is this reasonable?
    let nritz = min(nritz0, N)
    # Create Krylov subspace
    var V = newSparseMatrix[U, T](N, nritz, A.blocksize)
    var
        ilist: seq[U]
        jlist: seq[U]
        vlist: seq[T]
    let f = PI/T(N)
    for j in 0..<nritz:
        var s = T(0)
        for i in 0..<N:
            ilist.add(i)
            jlist.add(j)
            let v = sin(f*T(i*j))
            s+=v*v
            vlist.add(v)
        s = sqrt(s)
        for i in 0..<N:
            vlist[vlist.high-i]/=s
    V.set(ilist, jlist, vlist)
    let B = (V.transpose()*(A*V)).toTensor()
    #TODO: B is not symmetric?!? or is it?
    let esol = symeig(B, return_eigenvectors = true)
    let
        eigenval = esol[0]
        eigenvec = esol[1]
    # Transform eigenvectors from Krylov space to original
    #TODO: convert into eigenvectors of A y as y=V*x
    #TODO: is this correct? (order, tranpose, etc)
    #TODO: # of v as maxiter, nev as ?
    return (eigenval, V.toTensor()*eigenvec)

#[
when isMainModule:
    let A = diag[int, float64](@[1.0, 2, 3, 4, 5])
    #NOTE: library only works on symmetric matrices
    #echo "True value"
    #let res=symeig(A)
    #echo res[0]
    echo "DIY value"
    echo eigs_ritz(A)
]#
