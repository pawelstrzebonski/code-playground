import std/[math, tables, sequtils, sugar]
import arraymancer

type
    SparseMatrix*[U, T] = object
        ## This type is a blocked sparse matrix
        blocksize*: U
        ## Size of sub-component square matrices
        nx*: U
        ## Matrix size on first axis
        ny*: U
        ## Matrix size on second axis
        data*: Table[(U, U), Tensor[T]]
        ## Non-zero data of matrix, stored as coordinate => block table

func newSparseMatrix*[U, T](nx: U, ny: U, blocksize: U): SparseMatrix[U, T] =
    ## Create a new SparseMatrix of size `nx`-by-`ny` using blocks of size `blocksize`
    return SparseMatrix[U, T](nx: nx, ny: ny, blocksize: blocksize,
            data: initTable[(U, U), Tensor[T]]())

proc set*[U, T](sm: var SparseMatrix[U, T], xs: seq[U], ys: seq[U], values: seq[T]) =
    ## Assign values `values` to SparseMatrix `sm` at coordinates (`xs`, `ys`)
    assert len(xs) == len(ys) and len(xs) == len(values)
    let i = collect(newSeq):
        for x in xs:
            floorDiv(x, sm.blocksize)
    let j = collect(newSeq):
        for y in ys:
            floorDiv(y, sm.blocksize)
    let ir = collect(newSeq):
        for x in xs:
            assert x < sm.nx
            x mod sm.blocksize
    let jr = collect(newSeq):
        for y in ys:
            assert y < sm.ny
            y mod sm.blocksize
    for item in zip(zip(zip(i, j), zip(ir, jr)), values):
        let
            idxouter = item[0][0]
            idxinner = item[0][1]
            value = item[1]
        let
            x = idxinner[0]
            y = idxinner[1]
        if sm.data.hasKey(idxouter):
            var m = sm.data[idxouter]
            m[int(x), int(y)] = value
            sm.data[idxouter] = m
        else:
            var m = zeros[T](int(sm.blocksize), int(sm.blocksize))
            m[int(x), int(y)] = value
            sm.data[idxouter] = m

proc addTo*[U, T](self: var SparseMatrix[U, T], other: SparseMatrix[U, T]) =
    ## Add `SparseMatrix` `other` to `self`, overwriting `self`
    assert self.nx == other.nx and self.ny == other.ny and self.blocksize ==
            other.blocksize
    for k, v in other.data.pairs:
        if self.data.hasKey(k):
            self.data[k] += v
        else:
            self.data[k] = v

proc `+=`*[U, T](self: var SparseMatrix[U, T], other: SparseMatrix[U, T]) =
    ## Add `SparseMatrix` `other` to `self`, overwriting `self`
    addTo(self, other)

proc add*[U, T](self: SparseMatrix[U, T], other: SparseMatrix[U,
        T]): SparseMatrix[U, T] =
    ## Add `SparseMatrix` `other` to `self`
    return self.dup(addTo(other))

proc `+`*[U, T](self: SparseMatrix[U, T], other: SparseMatrix[U,
        T]): SparseMatrix[U, T] =
    ## Add `SparseMatrix` `other` to `self`
    return add(self, other)

proc subFrom*[U, T](self: var SparseMatrix[U, T], other: SparseMatrix[U, T]) =
    ## Subtract `SparseMatrix` `other` from `self`, overwriting `self`
    assert self.nx == other.nx and self.ny == other.ny and self.blocksize ==
            other.blocksize
    for k, v in other.data.pairs:
        if self.data.hasKey(k):
            self.data[k] -= v
        else:
            self.data[k] = v * -1

proc `-=`*[U, T](self: var SparseMatrix[U, T], other: SparseMatrix[U, T]) =
    ## Subtract `SparseMatrix` `other` from `self`, overwriting `self`
    subFrom(self, other)

proc sub*[U, T](self: SparseMatrix[U, T], other: SparseMatrix[U,
        T]): SparseMatrix[U, T] =
    ## Subtract `SparseMatrix` `other` to `self`
    return self.dup(subFrom(other))

proc `-`*[U, T](self: SparseMatrix[U, T], other: SparseMatrix[U,
        T]): SparseMatrix[U, T] =
    ## Subtract `SparseMatrix` `other` to `self`
    return sub(self, other)

proc mulTo*[U, T](self: var SparseMatrix[U, T], other: T) =
    ## Multiply `SparseMatrix` `self` by scalar `other`, overwriting `self`
    for k, v in self.data.mpairs:
        self.data[k] *= other

proc `*=`*[U, T](self: var SparseMatrix[U, T], other: T) =
    ## Multiply `SparseMatrix` `self` by scalar `other`, overwriting `self`
    mulTo(self, other)

proc mul*[U, T](self: var SparseMatrix[U, T], other: T) =
    ## Multiply `SparseMatrix` `self` by scalar `other`
    return self.dup(mulTo(other))

proc `*`*[U, T](self: var SparseMatrix[U, T], other: T) =
    ## Multiply `SparseMatrix` `self` by scalar `other`
    return mul(self, other)

proc mul*[U, T](self: SparseMatrix[U, T], other: SparseMatrix[U,
        T]): SparseMatrix[U, T] =
    ## Multiply `SparseMatrix` `self` with `other`, returning a new `SparseMatrix`
    ## as the result
    assert self.ny == other.nx and self.blocksize == other.blocksize
    var outmat = newSparseMatrix[U, T](self.nx, other.ny, self.blocksize)
    for kself, vself in self.data.pairs:
        for kother, vother in other.data.pairs:
            if kself[1] == kother[0]:
                let newkey = (kself[0], kother[1])
                if outmat.data.hasKey(newkey):
                    outmat.data[newkey]+=(vself*vother)
                else:
                    outmat.data[newkey] = (vself*vother)
    return outmat

proc `*`*[U, T](self: SparseMatrix[U, T], other: SparseMatrix[U,
        T]): SparseMatrix[U, T] =
    ## Multiply `SparseMatrix` `self` with `other`, returning a new `SparseMatrix`
    ## as the result
    return mul(self, other)

proc transpose*[U, T](self: SparseMatrix[U, T]): SparseMatrix[U, T] =
    ## Return a transposed copy of intput `SparseMatrix`
    var outmat = newSparseMatrix[U, T](self.ny, self.nx, self.blocksize)
    for kself, vself in self.data.pairs:
        let newkey = (kself[1], kself[0])
        outmat.data[newkey] = transpose(vself)
    return outmat

proc diag*[U, T](v: seq[T], blocksize: U = 4): SparseMatrix[U, T] =
    ## Return diagonal `SparseMatrix` given diagonal values
    let N = len(v)
    let i = toSeq(0..(N-1))
    var D = newSparseMatrix[U, T](N, N, blocksize)
    D.set(i, i, v)
    return D

proc toTensor*[U, T](self: SparseMatrix[U, T]): Tensor[T] =
    ## Convert `SparseMatrix` to dense `Tensor`
    var outmat = zeros[T](self.nx, self.ny)
    for k, v in self.data.pairs:
        let startx = k[0]*self.blocksize
        let stopx = min((k[0]+1)*self.blocksize, self.nx)
        let starty = k[1]*self.blocksize
        let stopy = min((k[1]+1)*self.blocksize, self.ny)
        outmat[startx..<stopx, starty..<stopy] = v[0..<min(self.blocksize,
                stopx-startx), 0..<min(self.blocksize, stopy-starty)]
    return outmat
