include sparse_vector
include sparse_matrix
import arraymancer

proc mul*[U, T](self: SparseMatrix[U, T], other: SparseVector[U,
        T]): SparseVector[U, T] =
    ## Multiply `SparseMatrix` `self` with `SparseVector` `other`, returning a new `SparseVector`
    ## as the result
    assert self.ny == other.nx and self.blocksize == other.blocksize
    var outvec = newSparseVector[U, T](self.nx, self.blocksize)
    for kself, vself in self.data.pairs:
        for kother, vother in other.data.pairs:
            if kself[1] == kother:
                let newkey = kself[0]
                if outvec.data.hasKey(newkey):
                    outvec.data[newkey]+=vself*vother
                else:
                    outvec.data[newkey] = vself*vother
    return outvec

proc `*`*[U, T](self: SparseMatrix[U, T], other: SparseVector[U,
        T]): SparseVector[U, T] =
    ## Multiply `SparseMatrix` `self` with `SparseVector` `other`, returning a new `SparseVector`
    ## as the result
    return mul(self, other)



when isMainModule:
    let
        N = 4
        BS = 2
    let A = toSeq(1..(N*N)).toTensor().reshape(N, N)
    let B = toSeq(1..N).toTensor()

    var
        xs: seq[int]
        ys: seq[int]
        vs: seq[float]
    for coord, value in A:
        xs.add(coord[0])
        ys.add(coord[1])
        vs.add(float(value))

    var sm = newSparseMatrix[int, float](N, N, BS)
    var sv = newSparseVector[int, float](N, BS)
    sm.set(xs, ys, vs)
    var
        xs2: seq[int]
        vs2: seq[float]
    for i in 0..<N:
        xs2.add(i)
        vs2.add(float(i+1))
    sv.set(xs2, vs2)
    #sm.add(sm)
    #sm+=sm
    #sm.sub(sm)
    #sm-=sm
    #sm.mul(1.23)
    #sm*=1.23
    #let sm2 = sm.mul(sm)
    #let sm3 = sm*sm2
    echo sm+sm
    echo A*A
    let sm2 = sm*sm
    echo sm2
    echo A*B
    let sv2 = sm*sv
    for k, v in sv2.data.pairs:
        echo k
        echo v

