import std/[math, tables, sequtils, sugar]
import arraymancer

type
    SparseVector*[U, T] = object
        ## This type is a blocked sparse vector
        blocksize*: U
        ## Size of sub-component tensors
        nx*: U
        ## Vector size
        data*: Table[U, Tensor[T]]
        ## Non-zero data of vector, stored as index => block table

func newSparseVector*[U, T](nx: U, blocksize: U): SparseVector[U, T] =
    ## Create a new SparseVector of size `nx` using blocks of size `blocksize`
    return SparseVector[U, T](nx: nx, blocksize: blocksize,
            data: initTable[U, Tensor[T]]())

proc set*[U, T](sm: var SparseVector[U, T], xs: seq[U], values: seq[T]) =
    ## Assign values `values` to SparseVector `sm` at indices `xs`,
    assert len(xs) == len(values)
    let i = collect(newSeq):
        for x in xs:
            floorDiv(x, sm.blocksize)
    let ir = collect(newSeq):
        for x in xs:
            assert x < sm.nx
            x mod sm.blocksize
    for item in zip(zip(i, ir), values):
        let
            idxouter = item[0][0]
            idxinner = item[0][1]
            value = item[1]
        if sm.data.hasKey(idxouter):
            var m = sm.data[idxouter]
            m[int(idxinner)] = value
            sm.data[idxouter] = m
        else:
            var m = zeros[T](int(sm.blocksize))
            m[int(idxinner)] = value
            sm.data[idxouter] = m

proc fromTensor*[U, T](self: var SparseVector[U, T], other: Tensor[T]) =
    assert other.rank == 1 and self.nx == other.shape[0]
    var
        ilist: seq[U]
        vlist: seq[T]
    for i, v in other:
        ilist.add(i)
        vlist.add(v)
    self.set(ilist, vlist)

proc toTensor*[U, T](self: SparseVector[U, T]): Tensor[T] =
    var outvec = zeros[T](self.nx)
    for idx, val in self.data.pairs:
        let start = idx*self.blocksize
        let stop = min((idx+1)*self.blocksize, self.nx)
        outvec[start..<stop] = val[0..<min(self.blocksize, stop-start)]
    return outvec

proc addTo*[U, T](self: var SparseVector[U, T], other: SparseVector[U, T]) =
    ## Add `SparseVector` `other` to `self`, overwriting `self`
    assert self.nx == other.nx and self.blocksize == other.blocksize
    for k, v in other.data.pairs:
        if self.data.hasKey(k):
            self.data[k] += v
        else:
            self.data[k] = v

proc `+=`*[U, T](self: var SparseVector[U, T], other: SparseVector[U, T]) =
    ## Add `SparseVector` `other` to `self`, overwriting `self`
    addTo(self, other)

proc add*[U, T](self: SparseVector[U, T], other: SparseVector[U,
        T]): SparseVector[U, T] =
    ## Add `SparseVector` `other` to `self`
    return self.dup(addTo(other))

proc `+`*[U, T](self: SparseVector[U, T], other: SparseVector[U,
        T]): SparseVector[U, T] =
    ## Add `SparseVector` `other` to `self`
    return add(self, other)

proc subFrom*[U, T](self: var SparseVector[U, T], other: SparseVector[U, T]) =
    ## Subtract `SparseVector` `other` from `self`, overwriting `self`
    assert self.nx == other.nx and self.blocksize == other.blocksize
    for k, v in other.data.pairs:
        if self.data.hasKey(k):
            self.data[k] -= v
        else:
            self.data[k] = v * -1

proc `-=`*[U, T](self: var SparseVector[U, T], other: SparseVector[U, T]) =
    ## Subtract `SparseVector` `other` from `self`, overwriting `self`
    subFrom(self, other)

proc sub*[U, T](self: SparseVector[U, T], other: SparseVector[U,
        T]): SparseVector[U, T] =
    ## Subtract `SparseVector` `other` to `self`
    return self.dup(subFrom(other))

proc `-`*[U, T](self: SparseVector[U, T], other: SparseVector[U,
        T]): SparseVector[U, T] =
    ## Subtract `SparseVector` `other` to `self`
    return sub(self, other)

proc mulTo*[U, T](self: var SparseVector[U, T], other: T) =
    ## Multiply `SparseVector` `self` by scalar `other`, overwriting `self`
    for k, v in self.data.mpairs:
        self.data[k] *= other

proc `*=`*[U, T](self: var SparseVector[U, T], other: T) =
    ## Multiply `SparseVector` `self` by scalar `other`, overwriting `self`
    mulTo(self, other)

proc mul*[U, T](self: SparseVector[U, T], other: T): SparseVector[U, T] =
    ## Multiply `SparseVector` `self` by scalar `other`
    return self.dup(mulTo(other))

proc `*`*[U, T](self: SparseVector[U, T], other: T): SparseVector[U, T] =
    ## Multiply `SparseVector` `self` by scalar `other`
    return mul(self, other)

proc dot*[U, T](self: SparseVector[U, T], other: SparseVector[U,
        T]): T =
    ## Dot product of a pair of `SparseVector`s
    assert self.nx == other.nx and self.blocksize == other.blocksize
    var dp = T(0)
    #TODO: this is a dumb way of iterating, redo this
    for kself, vself in self.data.pairs:
        for kother, vother in other.data.pairs:
            if kself == kother:
                dp+=dot(vself, vother)
    return dp
