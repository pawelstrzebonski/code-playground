include fdm
import arraymancer
include eigs

when isMainModule:
    var n: seq[float]
    for _ in 1..100:
        n.add(1.0)
    for _ in 1..100:
        n.add(3.0)
    for _ in 1..100:
        n.add(1.0)
    var A = fdm(n, 0.85, 0.01)
    var res = eigs_ritz(A)
    echo res[0]
    echo res[1].shape
    #[
    for k,v in L.data.pairs:
        echo k
        echo v
    ]#
