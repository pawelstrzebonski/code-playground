import sparse
import arraymancer
import std/math

proc laplacian[U, T](N: U, dx: T, blocksize: U = 4): SparseMatrix[U, T] =
    var
        i: seq[U]
        j: seq[U]
        v: seq[T]
    let a: T = -2/(dx*dx)
    let b: T = 1/(dx*dx)
    for idx in 0..(N-1):
        i.add(idx)
        j.add(idx)
        v.add(a)
        if idx > 0:
            i.add(idx-1)
            j.add(idx)
            v.add(b)
            i.add(idx)
            j.add(idx-1)
            v.add(b)
    var L = newSparseMatrix[U, T](N, N, blocksize)
    L.set(i, j, v)
    return L

proc fdm*[U, T](n: seq[T], wavelength: T, dx: T,
        blocksize: U = 4): SparseMatrix[U, T] =
    let k = 2*PI/wavelength
    let dX = dx*k
    var L = laplacian(len(n), dX, blocksize)
    var D = diag(n, blocksize)
    L-=D
    return L
