# Code Playground

A repository for miscellaneous side projects and scripts that don't quite warrant their own separate repositories.

## Organization

- `compute/`: Various projects with a more algorithmic/computational focus
- `nix/`: Scripts focusing on getting stuff to work with Nix/NixOS
- `photonics/`: Various projects related to photonics, optics, etc
