# llama.cpp on Nix

Various scripts for building and preparing [llama.cpp](https://github.com/ggerganov/llama.cpp) on NixOS.

The `default.nix` script will build the `llama.cpp` code. It is configured to only build the model quantization and execution binaries, and to build with specific BLAS and CPU extension support for compatibility with particular older CPUs.

The `get_weights_hf.sh` script will download the OpenLLaMA 3B model files from Hugging Face, as well as the model conversion python script from `llama.cpp`.

The `convert_weights.sh` script will convert the downloaded model files from PyTorch format to GGML format for compatibility with `llama.cpp`.

The `Makefile` wraps the commands for building/installing `llama.cpp`, running these scripts, as well as quantizing the GGML model weights.

**NOTE**: These scripts may be a bit hacky. They may need customization to run or be performant on different CPUs.

## TODOs

* download converted weights directly, e.g. https://huggingface.co/SlyEcho/open_llama_3b_ggml
