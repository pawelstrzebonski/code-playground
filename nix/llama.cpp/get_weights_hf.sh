#!/run/current-system/sw/bin/env nix-shell
#!nix-shell -i bash -p wget

# Reference: https://github.com/ggerganov/llama.cpp#using-openllama

# Download the original weights
#wget https://huggingface.co/openlm-research/open_llama_3b/resolve/main/pytorch_model.bin
if [ ! -f "tokenizer.model" ]; then
	wget https://huggingface.co/openlm-research/open_llama_3b/resolve/main/tokenizer.model
fi
if [ ! -f "pytorch_model.bin" ]; then
	wget https://huggingface.co/openlm-research/open_llama_3b/resolve/main/pytorch_model.bin
fi
if [ ! -f "config.json" ]; then
	wget https://huggingface.co/openlm-research/open_llama_3b/resolve/main/config.json
fi
if [ ! -f "tokenizer_config.json" ]; then
	wget https://huggingface.co/openlm-research/open_llama_3b/resolve/main/tokenizer_config.json
fi

# Download conversion script
if [ ! -f "convert.py" ]; then
	wget https://github.com/ggerganov/llama.cpp/raw/master/convert.py
fi
