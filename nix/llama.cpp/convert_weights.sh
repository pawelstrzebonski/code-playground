#! /usr/bin/env sh

# Convert original weights into GGML format
nix-shell -p python3 -p python3Packages.numpy python3Packages.sentencepiece --run "python convert.py ."

#TODO: quantize weights?
# --outtype q4_1
