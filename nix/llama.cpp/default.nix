{ pkgs ? import <nixpkgs> {} }:
with pkgs;

stdenv.mkDerivation rec {
	name = "llama.cpp";
	nativeBuildInputs = [ 
		pkg-config
		cmake 
		openblas # NOTE: building with OpenBLAS support
		#mkl  # NOTE: for use with Intel MKL
		#blis # NOTE: building with BLIS support
	];
	buildInputs = [
	];
	rev = "64639555ff93c8ead2b80becb49cc6b60aeac240";
	src = fetchFromGitHub {
		inherit rev;
		owner = "ggerganov";
		repo = "llama.cpp";
		sha256 = "sha256-nSobKxGDmGzq3X+fYK4Hv5Oc5hL5xOQD0nG+cmmkPYM=";
	};
	# NOTE: hacks to disable too new instruction sets for compatibility with older CPUs
	cmakeFlags = [ 
		"-DLLAMA_NATIVE=ON"
		"-DLLAMA_AVX=ON" 
		"-DLLAMA_AVX2=OFF" 
		"-DLLAMA_F16C=ON" 
		"-DLLAMA_FMA=OFF" 
		"-DLLAMA_BLAS=ON "# NOTE: building with BLAS support
		"-DLLAMA_BLAS_VENDOR=OpenBLAS" # NOTE: building with OpenBLAS support
		#"-DLLAMA_BLAS_VENDOR=FLAME" # NOTE: for use with BLIS
		#"-DLLAMA_BLAS_VENDOR=Intel10_64lp" # NOTE: for use with Intel MKL
	];
	# NOTE: avoid building all targets, only a few of interest
	makeFlags = [ "main" "quantize" ];
	# NOTE: copy-pasting specific built binaries into right location and giving them useful names
	installPhase = ''
		runHook preInstall
		mkdir -p $out/bin
		cp bin/main $out/bin/llama-cpp
		cp bin/quantize $out/bin/llama-cpp-quantize
		runHook postInstall
	'';
	meta = with lib; {
		description = "Port of Facebook's LLaMA model in C/C++";
		homepage = "https://github.com/ggerganov/llama.cpp";
		license = licenses.mit;
		#maintainers = with maintainers; [ pawelstrzebonski ];
		platforms = platforms.linux;
	};
}
