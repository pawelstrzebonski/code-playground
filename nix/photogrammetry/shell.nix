{ pkgs ? import <nixpkgs> {} }:
with pkgs;
mkShell rec {
    name = "shell";
    buildInputs = [
        colmap openmvs f3d
    ];
}
