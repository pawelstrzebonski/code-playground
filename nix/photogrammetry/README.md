# COLMAP + OpenMVS Photogrammetry on Nix/CPU

Most photogrammetry tools require NVIDIA GPUs and CUDA. This script enables CPU-only photogrammetry.

Various aspects of this script copied-from/inspired-by following sources/references:

* https://github.com/cdcseacave/openMVS/issues/180
* https://peterfalkingham.com/2017/05/26/photogrammetry-testing-11-visualsfm-openmvs/
* https://www.jeffgeerling.com/blog/2021/modeling-my-grandpa-3d-photogrammetry
* https://gist.githubusercontent.com/geerlingguy/7049523be6bbd3e7af3e9251a14b052b/raw/ba6758ab5e6e46e6a5b0b40d45bf8ffe479d397a/photogrammetry.sh 
