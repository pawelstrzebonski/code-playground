# LOVR on Nix

Some Nix scripts for building LOVR on NixOS.

The `default.nix` script should pull in the LOVR source and build it (not building successfully yet).

The `shell.nix` script should enable loading dependencies for building LOVR (you'll have to download source separately).
