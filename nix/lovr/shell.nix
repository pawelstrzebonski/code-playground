{ pkgs ? import <nixpkgs> {} }:
with pkgs;
mkShell rec {
    name = "shell";
    buildInputs = [
        cmakeMinimal
        pkg-config
    ];
    nativeBuildInputs = [
		xorg.libX11 xorg.libXrandr xorg.libXinerama xorg.libXcursor xorg.libXi curl
    ];
}
