{ pkgs ? import <nixpkgs> {} }:
with pkgs;

stdenv.mkDerivation rec {
	name = "lovr";
	buildInputs = [
		python3
	];
	nativeBuildInputs = [
        cmakeMinimal
        pkg-config
		xorg.libX11 xorg.libXrandr xorg.libXinerama xorg.libXcursor xorg.libXi
		curl
	];
	rev = "1cda2d59a7a40514ecd8a3aa050299bc0e816841";
	src = fetchFromGitHub {
		inherit rev;
		owner = "bjornbytes";
		repo = "lovr";
		sha256 = "sha256-szkeywxyBScr1DWbC1Wl6IrOhs1LwGKK4EkecZYW3GU=";
		#deepClone = true;
		fetchSubmodules = true;
	};
	meta = with lib; {
		description = "Lua Virtual Reality Framework";
		homepage = "https://lovr.org/";
		license = licenses.mit;
		#maintainers = with maintainers; [ pawelstrzebonski ];
		platforms = platforms.linux;
	};
}
