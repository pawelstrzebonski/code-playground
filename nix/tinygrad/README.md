# Tinygrad on Nix

Some Nix scripts for using tinygrad ML on NixOS.

The `default.nix` script should pull in the tinygrad source and build it (if you want hardware acceleration, you will need to add appropriate imports in that file).

The `shell.nix` script uses the `default.nix` script to create a development environment where tinygrad (and other defined packages) are available).
