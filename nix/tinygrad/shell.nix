with import <nixpkgs> {};
let tinygrad = callPackage ./default.nix {};
pythonEnv= python3.withPackages (ps: [ ps.numpy tinygrad ps.pandas ps.matplotlib]);
in mkShell {
  packages = [
	pythonEnv
  ];
}
