{ pkgs ? import <nixpkgs> {} }:
with pkgs;

python3Packages.buildPythonPackage rec {
	propagatedBuildInputs = [
		pkgs.python3Packages.numpy
		pkgs.python3Packages.requests
		pkgs.python3Packages.pillow
		pkgs.python3Packages.networkx
		pkgs.python3Packages.tqdm
		pkgs.python3Packages.pyyaml
		#TODO: what to build with Torch? torch?
		#TODO: what to build with CUDA? pycuda?
		#TODO: what to build with OpenCL? pyopencl?
		pkgs.python3Packages.pyopencl
		#TODO: what to build with LLVM? llvmlite?
		#pkgs.python3Packages.llvmlite
		#TODO: what to build with Meta? metal?
	];
	pname = "tinygrad";
	version = "0.8.0";
	src = python3.pkgs.fetchPypi {
		inherit pname version;
		hash = "sha256-AJRBmG3F8qtIo6bH7PbJLdoGhJadjDRp4TESm78YZ3c=";
	  };
	#NOTE: don't run tests on build, otherwise add more to buildInputs
	doCheck = false;
	meta = with lib; {
		homepage = "https://github.com/geohot/tinygrad";
		description = "You like pytorch? You like micrograd? You love tinygrad! heart";
		license = licenses.mit;
		maintainers = with maintainers; [ ];
	};
}
