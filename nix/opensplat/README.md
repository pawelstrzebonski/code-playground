# OpenSplat on Nix

Scripts for building [OpenSplat](https://github.com/pierotofy/OpenSplat) (for gaussian splatting) on NixOS.

Current script should build a CPU compute binary powered by libtorch.
