{ pkgs ? import <nixpkgs> {} }:
with pkgs;

stdenv.mkDerivation rec {
	name = "opensplat";
	nativeBuildInputs = [
		cmake
		libtorch-bin
		opencv
	];
	rev = "19310e735cd2ee0db49b22d0b6fd084a55336d35";
	src = fetchFromGitHub {
		inherit rev;
		owner = "pierotofy";
		repo = "OpenSplat";
		sha256 = "sha256-WAbyT7I1z87ntx3azn2V9JB1PKyqJ5Y00X3YoV+RAoo=";
	};
	installPhase = "
		runHook preInstall
		mkdir -p $out/bin
		cp opensplat $out/bin/opensplat
		runHook postInstall
	";
	meta = with lib; {
		description = "Free and open source 3D gaussian splatting in C++ with CPU and GPU (NVIDIA/AMD) support. Runs on Windows, Mac and Linux 💦";
		homepage = "https://github.com/pierotofy/OpenSplat";
		license = licenses.agpl3Only;
		#maintainers = with maintainers; [ pawelstrzebonski ];
		platforms = platforms.linux;
	};
}
