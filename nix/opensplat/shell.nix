{ pkgs ? import <nixpkgs> {} }:
with pkgs;
mkShell rec {
    name = "shell";
    buildInputs = [
        cmake libtorch-bin opencv
    ];
}
