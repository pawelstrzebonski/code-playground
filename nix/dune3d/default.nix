{ pkgs ? import <nixpkgs> {} }:
with pkgs;

gcc13Stdenv.mkDerivation rec {
	name = "dune3d";
	nativeBuildInputs = [ 
        pkgs.meson
        pkgs.ninja
        pkgs.cmake
        pkgs.librsvg
        pkgs.gobject-introspection
        pkgs.pkg-config
        pkgs.wrapGAppsHook
        python3Packages.pygobject3
	];
	buildInputs = [
        pkgs.eigen
        pkgs.glm
        pkgs.gtkmm4
        pkgs.libepoxy
        pkgs.libspnav
        pkgs.libuuid
        pkgs.opencascade-occt
	];
	rev = "55e52a4c5e9f3c022000cedb8a3abc2ef0a5a510";
	src = fetchFromGitHub {
		inherit rev;
		owner = "dune3d";
		repo = "dune3d";
		sha256 = "sha256-Z/kdOc/MbnnEyRsel3aZGndTAy1eCdAK0Wdta0HxaE4=";
	};
	meta = with lib; {
		description = "Dune 3D CAD application";
		homepage = "https://github.com/dune3d/dune3d/";
		license = licenses.gpl3;
		#maintainers = with maintainers; [  ];
		platforms = platforms.linux;
	};
}
