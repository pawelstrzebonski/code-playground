# 2x1 VCSEL FDM

Python code/scripts for simulating 2x1 photonic crystal VCSEL arrays using finite-difference method (FDM).

## Key Files

- `fdm.py`: basic implementation of FDM for 1D and 2D uniform grids
- `gamma.py`: functions for estimating confinement factors of modal fields. For single modes as well as for two modes beating against each-other (photon-photon resonance, PPR)
- `phc_vcsel.py`: provides a class for easy definition, simulation, and plotting of 2x1 PhC VCSEL arrays. Default parameters are similar to those in my PhD dissertation, ["Advances in Semiconductor Laser Mode and Beam Engineering"](http://hdl.handle.net/2142/113818).

If these files are run directly they will run some example/test code. If you want to use them, import the functions within them from a different script.

## Tech Notes

`fdm.py` uses `scipy`'s sparse matrix capabilities for faster/lower-memory matrix math and eigensolving.

`gamma.py` has functions for estimating the effect of PPR on the time-varying confinement factor. It does so by evaluating the confinement factor at a few relative modal phases and doing a basic sinusoidal fit. There may(?) be a better way of doing this but it seems to work well enough.

`phc_vcsel.py` uses `joblib` to cache FDM simulation results to files/directories. This may(?) lead to a large folder full of cache files if you are simulating a bunch of different structures (e.g. if you are trying to optimize the VCSEL array structure or parameters) but it should save time if you end up re-running the same simulations over and over again (e.g. you change your results processing/plotting scripts and need to re-run the simulations to update your figures).
