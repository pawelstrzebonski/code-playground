{ pkgs ? import <nixpkgs> {} }:
with pkgs;
mkShell rec {
    name = "shell";
    buildInputs = [
        python3Packages.scipy
        #python3Packages.numpy
        python3Packages.matplotlib
        python3Packages.pandas
        python3Packages.joblib
        python3Packages.black
	python3Packages.pylint
    ];
}
