"""Calculate confinement factors of modes."""
import numpy as np

# Stop complaining about variable naming
# pylint: disable=C0103


def confinement(mode: np.ndarray, amask: np.ndarray) -> float:
    """Calculate confinement of `mode` within region masked by `amask`."""
    assert mode.shape == amask.shape
    intensity = np.abs(mode) ** 2
    return np.sum(intensity * amask) / np.sum(intensity)


def confinements(modes: np.ndarray, amask: np.ndarray) -> np.ndarray:
    """Calculate confinement of all `modes` within region masked by `amask`."""
    assert modes.shape[:-1] == amask.shape
    intensity = np.abs(modes) ** 2
    axis = tuple(np.arange(modes.ndim, dtype=int)[:-1])
    return np.sum(intensity * np.expand_dims(amask, -1), axis=axis) / np.sum(
        intensity, axis=axis
    )


def temporal_confinement_array(
    mode1: np.ndarray, mode2: np.ndarray, amask: np.ndarray, n: int = 16
) -> np.ndarray:
    """Return lists phase and  confinement factor of beating of `mode1` with
    `mode2` within region masked by `amask` at `n` different relative phases."""
    assert mode1.shape == mode2.shape == amask.shape
    phases = np.arange(0, 2 * np.pi, 2 * np.pi / n)
    gammas = np.array(
        [confinement(mode1 + mode2 * np.exp(1j * phase), amask) for phase in phases]
    )
    return phases, gammas


def temporal_confinement(
    mode1: np.ndarray, mode2: np.ndarray, amask: np.ndarray, n: int = 16
) -> (float, float):
    """Estimate time-varying confinement factor of modes `mode1` and `mode2`
    beating against each-other within region masked by `amask`.

    Return (average, half-total-variation, relative phase) of confinement factor
    variation.

    Evalute at `n` different relative phases."""
    assert mode1.shape == mode2.shape == amask.shape
    phases, gammas = temporal_confinement_array(mode1, mode2, amask, n)
    # TODO: better way of finding variation, eg using similar to gphase?
    gavg = np.mean(gammas)
    gvar = (np.max(gammas) - np.min(gammas)) / 2
    gphase = np.angle(np.sum(np.exp(1j * phases) * gammas))
    return gavg, gvar, gphase


# pylint: disable=W0612
def main():
    """Run simple test cases (check function, not correctness)."""
    # Test 1D
    x = np.linspace(0, np.pi, 100)
    mode1 = np.cos(x)
    mode2 = np.sin(x)
    a = x < np.pi / 2
    print(confinement(mode1, a))
    modes = np.stack((mode1, mode2), axis=-1)
    print(confinements(modes, a))
    print(temporal_confinement(mode1, mode2, a))

    # Test 2D
    y = np.linspace(0, np.pi, 150)
    X, Y = np.meshgrid(x, y)
    mode1 = np.cos(X) * np.sin(Y)
    mode2 = np.cos(X) * np.cos(Y)
    a = (X < np.pi / 2) & (Y < np.pi / 2)
    print(confinement(mode1, a))
    modes = np.stack((mode1, mode2), axis=-1)
    print(confinements(modes, a))
    print(temporal_confinement(mode1, mode2, a))


if __name__ == "__main__":
    # Running as a script
    main()
