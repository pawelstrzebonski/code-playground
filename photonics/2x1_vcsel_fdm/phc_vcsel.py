"""2x1 PhC VCSEL array index structure generation."""
from joblib import Memory
import numpy as np
import matplotlib.pyplot as plt
import fdm
import gamma

# Setup caching
cachedir = "cache"
try:
    os.mkdir(cachedir)
except:
    pass
memory = Memory(cachedir, verbose=0)

# Stop complaining about variable naming
# pylint: disable=C0103
# Stop complaining about too many arguments in functions
# pylint: disable=R0913
# Stop complaining about too many instance attributes
# pylint: disable=R0902
# Stop complaining about too few public methods
# pylint: disable=R0903


def make_lattice(period: float):
    """Generate lists of hole coordinates"""
    # Define useful vectors
    v1 = np.array([0, 1]) * period
    v2 = np.array([np.sin(np.pi / 3), np.cos(np.pi / 3)]) * period
    v3 = v1 - v2
    # Offset to center the array
    offset = -v1 / 2
    # List of cavity core centers
    cores = [offset + v2, offset + v3]
    # List of inter-cavity small hole centers
    small_holes = [offset, offset + v1]
    # List of PhC large hole centers
    large_holes = []
    for i in range(0, 4):
        pt = offset - 2 * v1 + i * v2
        large_holes.append(pt)
    for i in range(-1, 4):
        pt = offset - v1 + i * v2
        large_holes.append(pt)
    for i in range(-2, 4):
        if i not in (0, 1):
            pt = offset + i * v2
            large_holes.append(pt)
    for i in range(-2, 4):
        if i not in (0, 1):
            pt = offset + i * v2 + v3
            large_holes.append(pt)
    for i in range(-1, 4):
        pt = offset + i * v2 + 2 * v3
        large_holes.append(pt)
    for i in range(0, 4):
        pt = offset + i * v2 + 3 * v3
        large_holes.append(pt)
    return cores, small_holes, large_holes


def make_index(
    simwidth: float,
    dx: float,
    nbulk: float,
    ncore: float,
    nhole: float,
    asymmetry: float,
    period: float,
    cores: list,
    small_holes: list,
    large_holes: list,
    mesawidth: float,
    fillfactor: float,
):
    """Generate simulation index array."""
    # Setup coordinates
    x = np.arange(-simwidth / 2, simwidth / 2 + dx, dx)
    X, Y = np.meshgrid(x, x)
    # Allocate index array to bulk
    n = np.full(X.shape, nbulk)
    # Allocate active region mask array to zeros
    a = np.zeros(X.shape)
    # Add cavity cores
    for i, pt in enumerate(cores):
        radius = period * (2 - fillfactor) / 2
        mask = (X - pt[0]) ** 2 + (Y - pt[1]) ** 2 <= radius**2
        n[mask] = ncore + asymmetry / 2 * (-1 if i == 0 else 1)
        a[mask] = i + 1
    # Add large PhC holes
    for pt in large_holes:
        radius = period * fillfactor / 2
        mask = (X - pt[0]) ** 2 + (Y - pt[1]) ** 2 <= radius**2
        n[mask] = nhole
    # Add small inter-cavity holes
    for pt in small_holes:
        radius = period * fillfactor / 4
        mask = (X - pt[0]) ** 2 + (Y - pt[1]) ** 2 <= radius**2
        n[mask] = nhole
    # Add hexagonal mesa etch
    for angle in np.arange(6) * np.pi / 3:
        radius = mesawidth / 2 * np.sqrt(3)
        Z = np.stack((X, Y), -1)
        v = np.array([np.cos(angle), np.sin(angle)])
        mask = np.dot((Z - radius * v), v) > 0
        n[mask] = nhole
    return X, Y, n, a


@memory.cache
def make_and_simulate(
    wavelength: float = 0.85,
    nev: int = 6,
    dx: float = 0.1,
    period: float = 4,
    fillfactor: float = 0.6,
    simwidth: float = 30,
    nbulk: float = 3.45102 + 1.96802e-3 * 1j,
    ncore: float = 3.44807189 - 5.66921e-4 * 1j,
    nhole: float = 1.84398 + 1.96802e-3 * 1j,
    asymmetry: float = 0,
    mesawidth: float = 20,
):
    """Run FDM simulation at `wavelength`, finding `nev` modes."""
    # Generate coordinates of holes
    cores, small_holes, large_holes = make_lattice(period)
    # Generate simulation array
    x, y, n, a = make_index(
        simwidth=simwidth,
        dx=dx,
        nbulk=nbulk,
        ncore=ncore,
        nhole=nhole,
        asymmetry=asymmetry,
        period=period,
        cores=cores,
        small_holes=small_holes,
        large_holes=large_holes,
        mesawidth=mesawidth,
        fillfactor=fillfactor,
    )
    # Run simulation
    neffs, modes = fdm.fdm2d(n, dx, dx, wavelength, ncore=ncore, nev=nev)
    # Sort in order of imaginary refractive index (modal gain)
    idx = np.argsort(np.imag(neffs))
    neffs, modes = neffs[idx], modes[:, :, idx]
    return neffs, modes


class PhC2x1:
    """2x1 PhC VCSEL array generation class.

    Once created, access the `.n` component for refractive
    index array, and `.active` component for activer region
    mask (each core will have different non-zero integral
    value).
    """

    # Attributes to be instatiated when simulated
    wavelength = None
    modes = None
    neffs = None
    # To be instantiated when confinement factor is calculated
    gammas = None

    def __init__(
        self,
        dx: float = 0.1,
        period: float = 4,
        fillfactor: float = 0.6,
        simwidth: float = 30,
        nbulk: float = 3.45102 + 1.96802e-3 * 1j,
        ncore: float = 3.44807189 - 5.66921e-4 * 1j,
        nhole: float = 1.84398 + 1.96802e-3 * 1j,
        asymmetry: float = 0,
        mesawidth: float = 20,
        wavelength: float = 0.85,
        nev: int = 6,
        simulate: bool = False,
    ):
        """
        Create a 2x1 PhC VCSEL array simulation structure.

        # Arguments:
        - `dx`: grid spacing
        - `period`: PhC lattice period
        - `fillfactor`: PhC hole diameter to lattice period ratio
        - `simwidth`: side length of simulation domain
        - `nbulk`: bulk refractive index
        - `ncore`: core refractive index
        - `nhole`: PhC hole and mesa field refractive index
        - `asymmetry`: index asymmetry between cavities
        - `mesawidth`: side length of mesa
        - `wavelength`: wavelength used for simulations
        - `nev`: number of modes to solve for
        - `simulate`: should FDM simulations be executed
        """
        assert 0 < fillfactor < 1
        assert mesawidth > 4 * period
        assert simwidth > 6 * period
        # Save parameters
        self.dx = dx
        self.period = period
        self.fillfactor = fillfactor
        self.simwidth = simwidth
        self.mesawidth = mesawidth
        self.nbulk = nbulk
        self.ncore = ncore
        self.nhole = nhole
        self.asymmetry = asymmetry
        # Generate coordinates of holes
        cores, small_holes, large_holes = make_lattice(period)
        # Generate simulation array
        self.x, self.y, self.n, self.active = make_index(
            simwidth=simwidth,
            dx=dx,
            nbulk=nbulk,
            ncore=ncore,
            nhole=nhole,
            asymmetry=asymmetry,
            period=period,
            cores=cores,
            small_holes=small_holes,
            large_holes=large_holes,
            mesawidth=mesawidth,
            fillfactor=fillfactor,
        )
        if simulate:
            self.neffs, self.modes = make_and_simulate(
                wavelength=wavelength,
                nev=nev,
                dx=dx,
                period=period,
                fillfactor=fillfactor,
                simwidth=simwidth,
                nbulk=nbulk,
                ncore=ncore,
                nhole=nhole,
                asymmetry=asymmetry,
                mesawidth=mesawidth,
            )

    def calculate_confinements(self):
        """Calculate confinement factors of each mode with each cavity.

        Can be accessed as `gammas` attribute."""
        confinements = []
        # Iterate over both cavities
        for i in range(2):
            # Calculate confinements for all modes
            confinements.append(gamma.confinements(self.modes, self.active == i + 1))
        # Convert to array and save
        self.gammas = np.array(confinements)

    def time_varying_confinement(self, modeidx1: int, modeidx2: int, cavity: int):
        """Calculate time-varying confinement factor between specified
        pair of modes, `modeidx1` and `modeidex2`, within given `cavity`.

        If cavity <0 then total confinement factor is analyzed.

        Return tuple of average, half-full-variation, and relative phase."""
        if cavity >= 0:
            amask = self.active == cavity
        else:
            amask = self.active > 0
        return gamma.temporal_confinement(
            self.modes[:, :, modeidx1],
            self.modes[:, :, modeidx2],
            amask,
        )

    def plot_array(self, points: bool = False):
        """Plot simulation arrays."""
        if points:
            pts = np.array(self.cores)
            plt.scatter(pts[:, 0], pts[:, 1])
            pts = np.array(self.small_holes)
            plt.scatter(pts[:, 0], pts[:, 1])
            pts = np.array(self.large_holes)
            plt.scatter(pts[:, 0], pts[:, 1])
            plt.legend(["Cores", "Small Holes", "Large Holes"])
            plt.tight_layout()
        else:
            plt.subplot(1, 3, 1)
            plt.imshow(
                np.real(self.n),
                extent=[self.x.min(), self.x.max(), self.y.min(), self.y.max()],
                cmap="magma",
            )
            plt.colorbar()
            plt.title("Real")
            plt.subplot(1, 3, 2)
            plt.imshow(
                np.imag(self.n),
                extent=[self.x.min(), self.x.max(), self.y.min(), self.y.max()],
                cmap="magma",
            )
            plt.colorbar()
            plt.title("Imag")
            plt.subplot(1, 3, 3)
            plt.imshow(
                self.active,
                extent=[self.x.min(), self.x.max(), self.y.min(), self.y.max()],
                cmap="magma",
            )
            plt.colorbar()
            plt.title("Active")
            plt.tight_layout()

    def plot_neff(self):
        """Plot modal effective indices."""
        plt.subplot(1, 2, 1)
        plt.scatter(
            range(len(self.neffs)),
            np.real(self.neffs),
        )
        plt.title("Real")
        plt.subplot(1, 2, 2)
        plt.scatter(
            range(len(self.neffs)),
            np.imag(self.neffs),
        )
        plt.title("Imag")
        plt.tight_layout()

    def plot_mode(self, modeidx: int, intensity: bool = True):
        """Plot desired mode."""
        if intensity:
            plt.imshow(
                np.abs(self.modes[:, :, modeidx]) ** 2,
                extent=[self.x.min(), self.x.max(), self.y.min(), self.y.max()],
                cmap="magma",
            )
            plt.colorbar()
            plt.tight_layout()
        else:
            plt.subplot(1, 2, 1)
            plt.imshow(
                np.abs(self.modes[:, :, modeidx]),
                extent=[self.x.min(), self.x.max(), self.y.min(), self.y.max()],
                cmap="magma",
            )
            plt.colorbar()
            plt.title("Magnitude")
            plt.subplot(1, 2, 2)
            plt.imshow(
                np.angle(self.modes[:, :, modeidx]),
                extent=[self.x.min(), self.x.max(), self.y.min(), self.y.max()],
                cmap="hsv",
            )
            plt.colorbar()
            plt.title("Phase")
            plt.tight_layout()

    def plot_gammas(self, modeidx1: int, modeidx2: int, n: int = 64):
        """Plot time-varying confinement factor from beating of
        modes `modeidx1` and `modeidx2`."""
        # Calculate gamma for each individual cavity
        p1, g1 = gamma.temporal_confinement_array(
            self.modes[:, :, modeidx1],
            self.modes[:, :, modeidx2],
            self.active == 1,
            n=n,
        )
        p2, g2 = gamma.temporal_confinement_array(
            self.modes[:, :, modeidx1],
            self.modes[:, :, modeidx2],
            self.active == 2,
            n=n,
        )
        # Sanity check on phases used
        assert (p1 == p2).all()
        # Estimate numerical properties of overall gamma variation
        gsol = gamma.temporal_confinement(
            self.modes[:, :, modeidx1], self.modes[:, :, modeidx2], self.active > 0, n=n
        )
        # Plot and label
        plt.plot(p1, g1)
        plt.plot(p1, g2)
        plt.plot(p1, g1 + g2)
        plt.xlabel("Phase")
        plt.ylabel("Confinement")
        plt.legend(["Cavity 1", "Cavity 2", "Total"])
        plt.title(f"Gamma={gsol[0]:.3f}+/-{gsol[1]:.3f}, phi={gsol[2]:.2f}")
        plt.tight_layout()


if __name__ == "__main__":
    # Running as a script
    phc = PhC2x1(simulate=True, dx=0.25)
    phc.plot_array()
    plt.show()
    plt.close()
    phc.plot_neff()
    plt.show()
    plt.close()
    phc.plot_mode(0)
    plt.show()
    plt.close()
    phc.plot_gammas(0, 1)
    plt.show()
    plt.close()
