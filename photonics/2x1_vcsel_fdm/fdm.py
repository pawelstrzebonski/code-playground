"""Finite Diffference Method waveguide mode solver."""
from scipy import sparse
import numpy as np

# Stop complaining about variable naming
# pylint: disable=C0103
# Stop complaining about too many arguments in functions
# pylint: disable=R0913


def laplacian1d(dx: float, nx: int):
    """1D Laplacian matrix for input of size `nx` and spacing `dx`."""
    assert nx > 1
    a = 1 / dx**2
    b = -2 / dx**2
    return sparse.diags(
        [np.full(nx - 1, a), np.full(nx, b), np.full(nx - 1, a)], [-1, 0, 1]
    )


def laplacian2d(dx: float, nx: int, dy: float, ny: int):
    """2D Laplacian matrix for input of size (`nx`, `ny`) and spacing (`dx`, `dy`)."""
    assert nx > 1 and ny > 1
    lx = laplacian1d(dx, nx)
    ly = laplacian1d(dy, ny)
    return sparse.kronsum(lx, ly)


def fdm_matrix1d(n: np.ndarray, dx: float, wavelength: float):
    """Create 1D FDM matrix for index array `n` with spacing `dx`
    and given `wavelength`."""
    k = 2 * np.pi / wavelength
    DX = dx * k
    L = laplacian1d(DX, n.shape[0])
    return sparse.diags([n**2], [0]) + L


def fdm_matrix2d(n: np.ndarray, dx: float, dy: float, wavelength: float):
    """Create 2D FDM matrix for index array `n` with spacing `dx`
    and given `wavelength`."""
    k = 2 * np.pi / wavelength
    DX = dx * k
    DY = dy * k
    L = laplacian2d(DX, n.shape[0], DY, n.shape[1])
    return sparse.diags([n.flatten() ** 2], [0]) + L


def fdm1d(
    n: np.ndarray,
    dx: float,
    wavelength: float,
    nev: int = 6,
    maxiter: int = 1_000,
    ncore=None,
):
    """Solve for waveguide modes of waveguide with index `n` and spacing `dx`
    at given `wavelength`.

    Pass # of desired mode solutions `nev` and iteration limit `maxiter` to eigensolver.

    If desired, give index value of waveguide core `ncore` (otherwise will default to
    highest real value in `n`)."""
    assert dx > 0 and wavelength > 0
    sigma = np.max(np.real(n)) ** 2 if ncore is None else ncore**2
    A = fdm_matrix1d(n, dx, wavelength)
    vals, vecs = sparse.linalg.eigs(A, sigma=sigma, k=nev, maxiter=maxiter)
    neffs = np.sqrt(vals)
    modes = vecs
    return neffs, modes


def fdm2d(
    n: np.ndarray,
    dx: float,
    dy: float,
    wavelength: float,
    nev: int = 6,
    maxiter: int = 1_000,
    ncore=None,
):
    """Solve for waveguide modes of waveguide with index `n` and spacing
    (`dx`, `dy`) at given `wavelength`.

    Pass # of desired mode solutions `nev` and iteration limit `maxiter` to eigensolver.

    If desired, give index value of waveguide core `ncore` (otherwise will default to
    highest real value in `n`)."""
    assert dx > 0 and dy > 0 and wavelength > 0
    sigma = np.max(np.real(n)) ** 2 if ncore is None else ncore**2
    A = fdm_matrix2d(n, dx, dy, wavelength)
    vals, vecs = sparse.linalg.eigs(A, sigma=sigma, k=nev, maxiter=maxiter)
    neffs = np.sqrt(vals)
    modes = vecs.reshape((*n.shape, -1))
    return neffs, modes


# pylint: disable=W0612
def main():
    """Run simple test cases (check function, not correctness)."""
    # Test 1D
    x = np.linspace(-3, 3, 100)
    dx = np.diff(x)[0]
    n = 3.4 + 0.1 * (abs(x) < 1)
    wavelength = 0.85
    neffs, modes = fdm1d(n, dx, wavelength)
    print(neffs)

    # Test 2D
    y = np.linspace(-4, 4, 200)
    dy = np.diff(y)[0]
    X, Y = np.meshgrid(x, y)
    n = 3.4 + 0.1 * (X**2 + Y**2 <= 1)
    neffs, modes = fdm2d(n, dx, dy, wavelength)
    print(neffs)


if __name__ == "__main__":
    # Running as a script
    main()
