import FDBPM, Plots, Statistics

DeltaX = 0.2
DeltaZ = 0.25
W = 4
wabc = 4
zmax = 20
x = -(W + wabc):DeltaX:(W+wabc)
z = 0:DeltaZ:zmax
Nx, Nz = length.([x, z])
abc(x, w) = (t = 1 - (x / w)^2 * im; t / abs(t))
n = [abs(x) <= 2 ? 3.0 : (abs(x) <= W ? 1 : abc(abs(x) - W, wabc / 4)) for x in x, z in z]
nmask = [abs(x) <= 2 ? 1 : 0 for x in x, z in z]

function sim_bpm(n; savename = "test.png")
    phi1 = [abs(x) <= 0.5 ? exp(-x^2) : 0.0 for x in x]
    phi2 =
        [abs(x - 1) <= 0.5 || abs(x + 1) <= 0.5 ? exp(-(abs(x) - 1)^2 / 4) : 0.0 for x in x]

    neff = maximum(real.(n))
    phifwd = FDBPM.bpm(n, phi1, DeltaX, DeltaZ, neff)
    @info abs(Statistics.cor(phi2, phifwd[:, end]))
    phibkw = reverse(FDBPM.bpm(reverse(n, dims = 2), phi2, DeltaX, DeltaZ, neff), dims = 2)
    dG = @. -8 * pi^2 * real(phifwd * conj(phibkw))
    Plots.plot(
        Plots.heatmap(z, x, abs.(n)),
        Plots.heatmap(z, x, abs.(phifwd)),
        Plots.heatmap(z, x, abs.(phibkw)),
        Plots.heatmap(z, x, real.(dG)),
        xlabel = "z",
        ylabel = "x",
        aspectratio = 1,
    ),
    dG
end

alpha = 1e-3
G = randn(size(n)) .* 0.1
_, dG = sim_bpm(n, savename = "initial.png")
anim = Plots.@animate for i = 1:50
    global n, G, dG
    G .+= @. dG * alpha
    #n[nmask.>0] .= (x -> x > 0 ? 3.0 : 2.0).(dG[nmask.>0])
    n .+= @. alpha * dG * nmask
    plt, dG = sim_bpm(n)
    plt
end
Plots.mp4(anim, "test.mp4", fps = 5)
