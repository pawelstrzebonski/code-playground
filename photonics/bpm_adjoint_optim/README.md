# BPM Adjoint Optimization

A Julia script attempting to use adjoint optimization methods and beam propagation method (BPM) to optimize a waveguide design for mode conversion. 

Depends on [FDBPM.jl](https://pawelstrzebonski.gitlab.io/FDBPM.jl).
